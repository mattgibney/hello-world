var express = require('express');
var app = express();
app.get('/', function (req, res) {
  res.status(200).send('ok');
});
var server = app.listen(3000, function () {
  var port = server.address().port;
  console.log('Hello World listening on port %s', port);
});
module.exports = server;
